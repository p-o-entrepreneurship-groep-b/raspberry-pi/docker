import LoraDevice
import threading


class LoraMonitor:

    def __init__(self, callback_message=None, callback_sent=None):
        self.lora_dev = LoraDevice(callback_message=callback_message)
        self.read_thread = threading.Thread(target=self.lora_dev.receive_loop)

    def send_message(self, message):
        self.lora_dev.transmit_message(message)

    def start_monitoring(self):
        self.read_thread.start()

    def terminate_reading(self):
        print('Stopping reading')
        self.lora_dev.stop()
        self.read_thread.join()
        print('Stopped reading')
