# Docker

This repo holds the essential components of the software on a SenCity sensor hub. Modules can be added at will. A `high-temp-module` has been added as illustration.
NOTE: The `docker-compose.yml`, `build` and `redis/redis.conf` files reference the `sound-recognition-module` and `sound-level-module` folders, that can be found in the repos by those names. They are referenced in this repo to illustrate where to make changes to add new modules, but are commented out.

## Scripts

There are a few scripts available to make development easier. `build` runs `docker-compose build`, along with taking care the template is present in the appropriate images. `run` runs `build`, and also starts up the server hub. Last but not least, `python3 add_module.py <module-name>` can be run to add the default infrastructure needed to run a newly added module.

