# In future, this script should be expanded to support limited access to IO devices (quite trivial, should just be added to redis.conf)

import sys
import hashlib

if len(sys.argv) != 2:
    print('Usage: `python3 add_module <mod-name>`')

mod_name = sys.argv[1]

# Update build script
with open('./build', 'r') as file:
    text = file.read()

with open('./build', 'w+') as file:
    new_text = 'cp -r ./template ./' + mod_name + '-module\n' + text + '\n' + 'rm -r ./' + mod_name + '-module/template'
    file.write(new_text)

print('Successfully updated build script')

docker_compose_addition = """        {0}-module:
                build: ./{0}-module
                environment:
                        - MODULE_ID={0}
                        - REDIS_USER={0}
                        - REDIS_PASS={0}-password

""".format(mod_name)

# Update docker-compose.yml
with open('docker-compose.yml', 'a') as file:
    file.write(docker_compose_addition)

print('Successfully updated docker-compose.yml')

pass_hash = hashlib.sha256((mod_name + '-password').encode()).hexdigest()
redis_conf_addition = '\nuser {0} on ~{0}:* -@all +subscribe +publish|module-{0} #{1}'.format(mod_name, pass_hash)

# Update redis/redis.conf
with open('redis/redis.conf', 'a') as file:
    file.write(redis_conf_addition)

print('Successfully updated redis/redis.conf')
