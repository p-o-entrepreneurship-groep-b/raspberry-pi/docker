import time
time.sleep(5)

import template

class HighTempModule (template.Module):
    def __init__(self):
        super().__init__()
        self.use('temperature')

    def run(self):
        while True:
            data = self.read_new('temperature')
            if data != None and int(data) >= 20:
                self.send_message('temp', int(data))
            time.sleep(.05)

HighTempModule().run()

