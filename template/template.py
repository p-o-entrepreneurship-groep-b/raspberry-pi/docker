import redis
import os

class Module:
    def __init__(self):
        self.__r = redis.Redis(host='redis', port=6379, username=os.getenv('REDIS_USER'), password=os.getenv('REDIS_PASS'))
        self.__p = self.__r.pubsub()
        self.__data = {}
        self.__id = os.getenv('MODULE_ID')

    def __fetch(self):
        message = self.__p.get_message()
        new = set()
        
        while message:
            if message['type'] == 'message':
                # print('Received data in template: {}'.format(message))
                channel = message['channel']
                prefix, name = channel[:6].decode('utf-8'), channel[7:].decode('utf-8')
                if prefix == 'device':
                    print('Device data')
                    new.add(name)
                    self.__data[name] = message['data']
            
            success = False
            while not success:
                try:
                    message = self.__p.get_message()
                    success = True
                except e:
                    print("Exception occurred: " + str(e))

        return new

    def use(self, name):
        print("Subscibing to " + name)
        self.__p.subscribe('device-{}'.format(name))
        self.__data[name] = None

    def get_var(self, name):
        print("Getting ENV variable" + name)
        self.__r.get("{}-{}".format(self.__id, name))

    def read(self, name):
        self.__fetch()
        return self.__data[name]

    def read_new(self, name):
        if name in self.__fetch():
            return self.__data[name]

    def send_message(self, name, data):
        # print('publishing [{}, {}]'.format(name, data))
        self.__r.publish(
            'module-{}'.format(self.__id),
            str([name, data]),
        )

