from enum import Enum
from multiprocessing import Process
import grovepi
import redis
import os
import time

r = redis.Redis(host='localhost', port=6379, username=os.getenv('REDIS_USER'), password=os.getenv('REDIS_PASS'))


class SensorType(Enum):

    DIGITAL = 1
    ANALOG = 2
    SELF_DEFINED = 3 # DHT sensor will not work with DIGITAL or ANALOG read
    # Self defined is misschien wat gek, maar deze gebruiken elk een andere library...
    # Meeste sensoren zullen generic gebruiken, maar sensoren zoals bv de DHT Temp-Hum maken gebruik van 1-wire protocol
    # Detectie is voldoende, de software groep kaan dana eventueel een optie geven self defined te gebruiken?


# General sensor class to read an analog/digital signal
class Sensor:

    def __init__(self, port, self_def_read_func=None):

        letter = port[0] # either "A" or "D"
        number = port[1]

        self.port = port

        # Defines the type of the sensor based on the Enum SensorType
        if letter == "A":
            self.type = SensorType.ANALOG
        elif letter == "D":
            self.type = SensorType.DIGITAL
        else:
            self.type = SensorType.SELF_DEFINED

        self.name = input("What sensor has just been attached to {}? ".format(port))

        # On which pin on the raspberry pi board is the sensor connected
        self.pin = int(number)

        if type == SensorType.SELF_DEFINED:
            if self_def_read_func is not None:
                self.self_def_read_func = self_def_read_func
            else:
                raise Exception("When a self defined type is used, you have to define your own read function as argument 'self_def_read_func'")

        # The process of this sensor that will continually read
        self.p = Process(target=self.read_and_publish)

    def read_self_def(self):

        # GROTE VRAAG! Kunnen we iets maken zodat we soort van pipelining uitvoeren van custom libraries op GPIO pins
        # naar die custom libs op GrovePi pins?

        # Ik heb de indruk dat dat niet mogelijk is... Is ook maar prototype
        return self.self_def_read_func()

    def read(self):

        grovepi.pinMode(self.pin, "INPUT")

        if self.type == SensorType.ANALOG:
            return grovepi.analogRead(self.pin)
        elif self.type == SensorType.DIGITAL:
            return grovepi.digitalRead(self.pin)
        else:
            return self.read_self_def()

    def read_and_publish(self):
        # Read from sensor and publish to redis
        while(True):
            r.publish("device-" + self.name, self.read())
            print("Publish {} on port {}: {}".format(self.name, self.port, self.read()))
            time.sleep(0.3)

    def start_reading(self):
        # Start reading process
        self.p.start()

    def stop_reading(self):
        # End reading process
        self.p.terminate()

