import numpy as np
import librosa
import matplotlib.pyplot as plt
import noisereduce as nr
from keras.models import model_from_json
from sklearn.preprocessing import LabelEncoder
import IPython
import os
import redis
from IOInterface.NumpyRedis import fromRedis

# Load segment audio classification model

model_path = r"Models/"
model_name = "audio_NN_New2020_02_27_12_46_42_acc_93.33"

# Model reconstruction from JSON file
with open(model_path + model_name + ".json", 'r') as f:
    model = model_from_json(f.read())

# Load weights into the new model
model.load_weights(model_path + model_name + '.h5')

# Replicate label encoder
lb = LabelEncoder()
lb.fit_transform(['GlassBreak', 'Scream', 'Crash', ])

# Some Utils

# Plot audio with zoomed in y axis

def plotAudio(output):
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(20, 10))
    plt.plot(output, color='blue')
    ax.set_xlim((0, len(output)))
    ax.margins(2, -0.1)
    plt.show()

# Plot audio


def plotAudio2(output):
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(20, 4))
    plt.plot(output, color='blue')
    ax.set_xlim((0, len(output)))
    plt.show()


def minMaxNormalize(arr):
    mn = np.min(arr)
    mx = np.max(arr)
    return (arr-mn)/(mx-mn)


def predictSound(X):
    # Empherically select top_db for every sample
    clip, index = librosa.effects.trim(
        X, top_db=20, frame_length=512, hop_length=64)
    stfts = np.abs(librosa.stft(clip, n_fft=512,
                                hop_length=256, win_length=512))
    stfts = np.mean(stfts, axis=1)
    stfts = minMaxNormalize(stfts)
    result = model.predict(np.array([stfts]))
    print(result)
    predictions = [np.argmax(y) for y in result]
    if np.max(result) > 0.8:
        print(lb.inverse_transform([predictions[0]])[0])
    else:
        print("other")
    plotAudio2(clip)

# load redis connection
r = redis.Redis(host='redis', port=6379, username=os.getenv('REDIS_USER'), password=os.getenv('REDIS_PASS'))
r.subscribe("device-microphone")

# noise window

noise_sample = np.frombuffer(r.get_message(), dtype=np.float32)[-10000:]
print("Noise Sample")
plotAudio2(noise_sample)
loud_threshold = np.mean(np.abs(noise_sample)) * 10
print("Loud threshold", loud_threshold)
audio_buffer = []
near = 0

while(True):
    # Read chunk and load it into numpy array.
    current_window = np.frombuffer(r.get_message(), dtype=np.float32)
    #current_window = np.frombuffer(data, dtype=np.float32)

    # Reduce noise real-time
    current_window = nr.reduce_noise(
        audio_clip=current_window, noise_clip=noise_sample, verbose=False)

    if(audio_buffer == []):
        audio_buffer = current_window
    else:
        if(np.mean(np.abs(current_window)) < loud_threshold):
            print("Inside silence reign")
            if(near < 10):
                audio_buffer = np.concatenate((audio_buffer, current_window))
                near += 1
            else:
                predictSound(np.array(audio_buffer))
                audio_buffer = []
                near
        else:
            print("Inside loud reign")
            near = 0
            audio_buffer = np.concatenate((audio_buffer, current_window))

