import numpy as np
import pyaudio
#
import redis
import os

r = redis.Redis(host='localhost', port=6379, username=os.getenv('REDIS_USER'), password=os.getenv('REDIS_PASS'))
p = pyaudio.PyAudio()

for i in range(p.get_device_count()):

    print(p.get_device_info_by_index(i))
form_1 = pyaudio.paFloat32  # 32-bit resolution
chans = 1  # 1 channel
samp_rate = 44100 #22050  # 44100  # 44.1kHz sampling rate
chunk = 44100  # OLD VALUE 4096  # 2^12 samples for buffer
dev_index = 2  # device index found by p.get_device_info_by_index(ii)

audio = pyaudio.PyAudio()  # create pyaudio instantiation

# create pyaudio stream

stream = audio.open(format=form_1, rate=samp_rate, channels=chans, \
                    input_device_index=dev_index, input=True, \
                    frames_per_buffer=chunk)

# loop through stream and append audio chunks to frame array
# for ii in range(0,int((samp_rate/chunk)*record_secs)):
#     frames.append(data)

while True:
    try:

        data = stream.read(chunk, exception_on_overflow=False)
        # print(type(data))
        r.publish("device-microphone", data)
    except KeyboardInterrupt:
        print("Received keyboard interrupt")
        break

# stop the stream, close it, and terminate the pyaudio instantiation
stream.stop_stream()
stream.close()
audio.terminate()
