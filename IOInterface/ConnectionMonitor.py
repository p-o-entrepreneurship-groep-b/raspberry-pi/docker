# ConnectionMonitor class to detect changes in connected sensors

import RPi.GPIO as GPIO
from time import sleep

from enum import Enum


# The ConnectionMonitor will use the GPIO pins of the raspberry pi.
# Only a physical connection with the GrovePi hat!!

# Lets say 13, 16 and 19 are for A0, A1, A3
# and 20, 21 and 26 for D2, D3, D4
# These are pins of type BCM
# If extra pins are added in the Port class, they should work out of the box
# The names of the pins will be transmitted to the back-end: A0, A1, ...

class Port(Enum):
    A0 = 13
    A1 = 19
    A2 = 26
    D2 = 16
    D3 = 20
    D4 = 21


class ConnectionMonitor:
    """
    BCM PINS!!

    We use a static list with the pins we're using to detect if a sensor is connected to the GrovePi shield.
    A GrovePi board has 3 analog and 7 digital pins. So we need 10 inputs on the raspberry pi.
    """

    # Lets start with these BCM pins
    pins = [item.value for item in Port]

    """
    The connection monitor is initialised with all GPIO pins that act as digital inputs to monitor if a sensor or
    another device is connected to the raspberry pi.
    """

    def __init__(self, connected_callback=None, disconnected_callback=None):
        GPIO.setmode(GPIO.BCM)

        # Instead of adding our own resistor to the sensor detection system, the pi will use his built in resistor as defined.
        # This will set all pins as INPUT pull-down.
        for p in self.pins:
            GPIO.setup(p, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

        if connected_callback:
            self.connected_callback = connected_callback
        else:
            self.connected_callback = self.__default_connected_callback__

        if disconnected_callback:
            self.disconnected_callback = disconnected_callback
        else:
            self.disconnected_callback = self.__default_disconnected_callback__

    """"Default message when a pin is connected"""
    @staticmethod
    def __default_connected_callback__(ports):
        print("These ports are connected: {}".format(ports))

    """"Default message when a pin is disconnected"""
    @staticmethod
    def __default_disconnected_callback__(ports):
        print("These ports are disconnected: {}".format(ports))

    """Get the name of the pin"""
    @staticmethod
    def __get_port_name__(pin):
        for item in Port:
            if item.value == pin:
                return item.name
        return None

    """Get the names of all pins that are connected"""
    def get_connected_ports(self):

        connected_pins = set()
        for p in self.pins:
            if self.__poll_pin__(p):
                # sensor connected
                connected_pins.add(self.__get_port_name__(p))

        return connected_pins

    """Check if this pin is high/on"""
    @staticmethod
    def __poll_pin__(pin):
        # Maybe we need to poll a few times to be sure?
        total = 0
        for _ in range(3):
            total += GPIO.input(pin)
            sleep(0.02)
        return round(total / 3)

    """"Calculate the difference between two sets"""
    @staticmethod
    def diff(first, second):
        A = set(first)
        B = set(second)
        return A - B

    """Loop and check if ports are connected/disconnected. If yes give a callback"""
    def loop(self):
        ports = self.get_connected_ports()
        while True:
            update_ports = self.get_connected_ports()

            added_ports = self.diff(update_ports, ports)
            removed_ports = self.diff(ports, update_ports)

            if len(added_ports) != 0:
                # Verschillend aantal verbonden sensoren => nieuwe poort gevonden
                # Call back de nieuwe poort
                self.connected_callback(added_ports)

            if len(removed_ports) != 0:
                # Call back dat er een poort weg is
                self.disconnected_callback(removed_ports)

            ports = update_ports

