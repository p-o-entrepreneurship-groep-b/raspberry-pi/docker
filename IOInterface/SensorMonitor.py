from ConnectionMonitor import ConnectionMonitor
from Sensor import Sensor
import redis
import os

r = redis.Redis(host='localhost', port=6379, username=os.getenv('REDIS_USER'), password=os.getenv('REDIS_PASS'))

class SensorMonitor:

    def __init__(self):
        # Dictionary with all the connected ports and sensors
        self.sensors = {}
        # The connection monitor to check for new connections
        self.cm = ConnectionMonitor(self.sensor_activated, self.sensor_deactivated)
        # Activate sensors that were already connected before sensor monitor was created
        self.sensor_activated(self.cm.get_connected_ports())

    def start_monitor_loop(self):
        self.cm.loop()

    def sensor_activated(self, ports):

        # Create new sensor on this port
        for port in ports:
            print("connecting to redis...")
            r.publish("device-info", "pin-added:{}".format(port))
            print("device-info: pin-added:{}".format(port))
            s = Sensor(port)
            # Add it to the dictionary of sensors
            self.sensors[port] = s
            s.start_reading()

    def sensor_deactivated(self, ports):

        for port in ports:
            print("connecting to redis...")
            r.publish("device-info", "pin-removed:{}".format(port))
            print("device-info: pin-removed:{}".format(port))
            # Get the sensor on the pin and stop reading
            self.sensors.get(port).stop_reading()
            # Remove the sensor from the dictionary
            del self.sensors[port]

    def get_connected_sensors(self):
        return self.sensors
