import time
time.sleep(5)

import template

class SystemModule (template.Module):
    def __init__(self):
        super().__init__()
        self.use('info')

    def process_message(self, data):
        message_type, message = data.split(':', 1)
        if message_type == 'pin-added':
            print("Sending pin-added message!")
            self.send_message('pin-added', data)

        elif message_type == 'pin-removed':
            print("Sending pin-removed message!")
            self.send_message('pin-removed', data)

    def run(self):
        while True:
            data = self.read_new('info')
            if data != None:
                self.process_message(data.decode())
            time.sleep(.1)

SystemModule().run()
