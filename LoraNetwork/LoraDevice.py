import re
import subprocess
import pty
import select
import threading
from time import sleep

#!/usr/bin/env python3
#
# Written 2017, 2019 by Tobias Brink
#
# To the extent possible under law, the author(s) have dedicated
# all copyright and related and neighboring rights to this software
# to the public domain worldwide. This software is distributed
# without any warranty.
#
# You should have received a copy of the CC0 Public Domain
# Dedication along with this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.

import errno
import os
import signal

# Set signal handler for SIGINT.
signal.signal(signal.SIGINT, lambda s,f: print("received SIGINT"))


class OutStream:
    def __init__(self, fileno):
        self._fileno = fileno
        self._buffer = b""

    def read_lines(self):
        try:
            output = os.read(self._fileno, 1000)
        except OSError as e:
            if e.errno != errno.EIO: raise
            output = b""
        lines = output.split(b"\n")
        lines[0] = self._buffer + lines[0] # prepend previous
                                           # non-finished line.
        if output:
            self._buffer = lines[-1]
            finished_lines = lines[:-1]
            readable = True
        else:
            self._buffer = b""
            if len(lines) == 1 and not lines[0]:
                # We did not have buffer left, so no output at all.
                lines = []
            finished_lines = lines
            readable = False
        finished_lines = [line.rstrip(b"\r").decode()
                          for line in finished_lines]
        return finished_lines, readable

    def fileno(self):
        return self._fileno





class LoraDevice:

    path = ""

    stop_doing_what_youre_doing = False


    def execute_command(self,command,message=""):
        # Start the subprocess.
        out_r, out_w = pty.openpty()
        err_r, err_w = pty.openpty()
        self.process = subprocess.Popen([self.path+"rpi-direct/dragino_lora_app/dragino_lora_app", command, message], stdout=out_w,
                                stderr=err_w)
        #print(process)

        os.close(out_w)  # if we do not write to process, close these.
        os.close(err_w)

        fds = {OutStream(out_r), OutStream(err_r)}

        while fds:
            # Call select(), anticipating interruption by signals.
            while True:
                try:
                    rlist, _, _ = select.select(fds, [], [])
                    break
                except InterruptedError:
                    continue
            # Handle all file descriptors that are ready.
            for f in rlist:
                lines, readable = f.read_lines()
                # Example: Just print every line. Add your real code here.
                for line in lines:
                    receive_search = re.search(r'(Payload:)\s([^\n]+)', line)
                    sent_search = re.search(r'(send:)\s([^\n]+)', line)
                    if receive_search is not None:
                        self.__message_callback__(receive_search.group(2))
                    if sent_search is not None:
                        self.__sent_callback__(sent_search.group(2))
                    #print(line)

                if not readable:
                    # This OutStream is finished.
                    fds.remove(f)

    def __init__(self, path=None,callback_message=None,callback_sent=None):
        if path is not None:
            self.path = path
        if callback_message is not None:
            self.__message_callback__ = callback_message
        else:
             self.__message_callback__ = self.__default_message_callback__

        if callback_sent is not None:
            self.__sent_callback__ = callback_sent
        else:
            self.__sent_callback__ = self.__default_sent_callback__

    def __default_message_callback__(self, message):
        print("LoraDevice callback: Received a message! Message: {}".format(message))

    def __default_sent_callback__(self,message=""):
        print("LoraDevice callback: Message sent!")


    def receive_loop(self):
        self.execute_command("Receive")

        ## OUTPUT:

        # SX1276 detected, starting.
        # Listening at SF7 on 868.100000 Mhz.
        # ------------------
        # Packet RSSI: -76, RSSI: -94, SNR: 10, Length: 7
        # Payload: KutLora
        # Packet RSSI: -71, RSSI: -109, SNR: 8, Length: 7
        # Payload: KutLora



    def transmit_message(self,message):
        self.execute_command(command="sender",message=message)
        ## OUTPUT:

        # SX1276 detected, starting.
        # Send packets at SF7 on 868.100000 Mhz.
        # ------------------
        # Length of text: 100
        # send: Het werkt!
        # send: Het werkt!

    def stop(self):
        self.process.kill()



if __name__ == '__main__':
    # Voorbeeld hoe te gebruiken
    dev = LoraDevice()
    x = threading.Thread(target=dev.receive_loop)
    x.start()
    sleep(10)
    dev.stop()
    x.join()
