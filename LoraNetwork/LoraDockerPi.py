import base64
import json
from time import *
from datetime import datetime
from LoraMonitor import LoraMonitor
import redis, os

time.sleep(5)

print('alive')

r = redis.Redis(host='127.0.0.1', port=6379, username=os.getenv('default'), password=os.getenv('KortijkSuckt'))
p = r.pubsub()

p.psubscribe('module-*')

guid = None


def incoming_lora_message(message):
    ## Received from backend

    print("Received: " + message)

    data = json.loads(message)

    # Check if guid matches this one
    if data['guid'] != guid:
        return

    # put new settings in redis
    # TODO


lora_monitor = LoraMonitor(callback_message=incoming_lora_message)


def send_to_lora(module_id, payload):
    # Sensor Hub --> Backend
    """
    {
        "hub": 0,
        "timestamp": "2020-03-24T14:25:16.982Z",
        "module_id": "string",
        "payload": "string"
    }
    """
    # id=None, hub=None, hub_guid=None, timestamp=None, module_guid=None, module=None,
    # payload=None, local_vars_configuration=None

    message = {
        "hub_guid": guid,
        #"timestamp": datetime.now().isoformat(),
        "module_guid": module_id,
        "payload": base64.b64encode(str.encode(str(payload)))
    }

    lora_monitor.send_message(json.dump(message))


def new_data_redis():
    msgs = []
    # check new sensor data
    message = p.get_message()
    # {'pattern': None, 'type': 'subscribe', 'channel': 'my-first-channel', 'data': 2L}
    while message:

        if message['type'] == 'message':
            msgs.append([guid, message['channel'][7:], message['data']])

        message = p.get_message()

    return msgs


def main():

    lora_monitor.start_monitoring()

    while True:
        data = new_data_redis()
        if len(data) != 0:
            lora_monitor.terminate_reading()
            sleep(0.1)
            for sensor_data in data:
                hub_id, module_id, payload = sensor_data
                send_to_lora(hub_id, module_id, payload)

            sleep(0.1)
            lora_monitor.start_monitoring()
