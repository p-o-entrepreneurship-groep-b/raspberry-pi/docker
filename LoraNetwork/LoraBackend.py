import json
from time import sleep
import requests as req
import sencity_client
from sencity_client.rest import ApiException

from LoraMonitor import LoraMonitor


def incoming_lora_message(message):
    ## Received an incident
    # Do a post request
    # Assumes following structure:
    """
    {
        "hub_guid": 0,
        "module_guid": "string",
        "payload": base64
    }
    """
    mes = '{"hub_guid": "19147c4f-08c5-433c-bb8a-e6b963643025","module_guid": "b15a5730-7786-4a5a-b00f-dfc1e3583909","timestamp": "2015-10-22T19:50:08","payload": "SEVMTE9fV09STEQ="}'
    print("Received the following message:")
    print(message)
    msg = json.loads(mes)
#    print(json.dumps(msg, indent=4, sort_keys=True))
    global api_instance
    incident = sencity_client.Incident(hub_guid=msg['hub_guid'], module_guid=msg['module_guid'],
                                      payload=msg['payload'], timestamp=msg['timestamp'])

    try:
        print("Incoming message from SensorHub: " + message)
        api_response = api_instance.incidents_create(incident)
        print("Response from backend while reporting an incident:\n" + api_response)
    except ApiException as e:
        print("Exception when calling IncidentsApi->incidents_create: %s\n" % e)


lora_monitor = LoraMonitor(callback_message=incoming_lora_message)


def poll_backend_for_changes():
    # Settings --> Sensor Hub

    global api_instance_mod
    unix_timestamp = '0'  # str |
    changes = []
    try:

        api_response = api_instance_mod.modules_settings_changed_read(unix_timestamp)
        print("Check for changes on backend...")
        print(api_response)
        for changed_setting in api_response:
            print(changed_setting)
            # print(changed_setting['key'])
            # print(changed_setting['value'])
            # print(changed_setting['module_guid'])
            # print(changed_setting['hub_guid'])
            changes.append(json.dump(changed_setting))

        print("Check done!")
    except ApiException as e:
        print("Exception when calling ModulesApi->modules_settings_changed_read: %s\n" % e)

    return changes


configuration = sencity_client.Configuration()
# Configure HTTP basic authorization: Basic
#configuration.username = 'YOUR_USERNAME'
#configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to http://localhost:8000
configuration.host = "http://192.168.0.111:8000"
# Enter a context with an instance of the API client
with sencity_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance_mod = sencity_client.ModulesApi(api_client)
    api_instance = sencity_client.IncidentsApi(api_client)

lora_monitor.start_monitoring()

try:
    while True:
        data = poll_backend_for_changes()
        if len(data) != 0:
            lora_monitor.terminate_reading()
            sleep(0.1)
            for setting in data:
                lora_monitor.send_message(setting)
            sleep(0.1)
            lora_monitor.start_monitoring()
        sleep(5.5)  # Om halve second gaat ie lezen of er nieuwe dingen zijn
except:
    print("exception")
