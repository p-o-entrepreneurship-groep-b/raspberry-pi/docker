import time
time.sleep(5)

import redis, random, os
r = redis.Redis(host='redis', port=6379, username=os.getenv('REDIS_USER'), password=os.getenv('REDIS_PASS'))

import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)

while True:
    value = random.randint(0, 32)
    r.publish('device-temperature', value)

    if random.random() < .1:
        r.publish('device-info', 'pin-added:' + random.choice(('A', 'D')) + str(random.randint(0,9)))

    elif random.random() < .1:
        r.publish('device-info', 'pin-removed:' + random.choice(('A', 'D')) + str(random.randint(0,9)))

    time.sleep(.5)
